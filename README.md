# rixfm-dl

## Description

This is a tool to download a set amount of seconds from Rix FM's web stream.
 The theoretical max length per download is 18446744073709551615 seconds (or
 around 584542046090 years, if my calculations are correct), however I imagine
 you'd run into other issues before you hit that number.

Note that in other to run this you need Cargo to download dependencies and
 rustc to compile. You can get these via [rustup](https://rustup.rs/).

## Usage

### Syntax

`cargo run <number of seconds> <resulting filename>`

### Example usage

This will seconds 120 seconds and output the file bad-song.mp3 given that there
 is not already a file called bad-song.mp3, in which case it will append a
 number to the filename.

`cargo run 120 bad-song`

