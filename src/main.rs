extern crate reqwest;
extern crate regex;

use std::env;
use std::io;
use std::io::Read;
use std::fs::File;
use std::path::Path;
use regex::Regex;

const RIXFM_URL: &str = "http://fm01-ice.stream.khz.se/fm01_mp3";
const RIXFM_DATA_PER_SECOND: u64 = 16000;
const FILE_EXTENSION: &str = "mp3";

fn main() {
    let arguments: Vec<_> = env::args().collect();

    if arguments.len() == 3 {
        let filename = check_filename(arguments[2].clone());
        start_dl(arguments[1].parse().unwrap(), &filename);
    } else {
        eprintln!("Invalid amount of arguments supplied ({}, should have been 2) :(. Correct usage is rixfm-dl <number of seconds to download> <filename>", arguments.len() - 1);
    }

    println!("Program execution has finished");
}

fn start_dl(seconds: u64, filename: &str) {
    let mut response = reqwest::get(RIXFM_URL)
        .expect(format!("Failed to request {} stream", FILE_EXTENSION).as_ref())
        .take(RIXFM_DATA_PER_SECOND * seconds);

    let mut output = File::create(format!("{}.{}", filename, FILE_EXTENSION))
        .expect("Failed to create file!");

    io::copy(&mut response, &mut output)
        .expect(format!("Failed to copy {} stream to file", FILE_EXTENSION).as_ref());
}

fn check_filename(filename: String) -> String {
    let filepath: String = format!("{}.{}", filename, FILE_EXTENSION);
    let find_file_number = Regex::new(r"\d*$")
        .unwrap();

    let path_object: &Path = Path::new(&filepath);

    if !path_object.exists() {
        filename
    } else {
        let match_opt = find_file_number
            .captures(&filename)
            .unwrap()
            .get(0);

        match match_opt {
            Some(match_value) => {
                match match_value.as_str().parse::<u16>() {
                    Ok(match_integer) => {
                        let de_suffixed_filename = remove_suffix(&filename, &format!("-{}", &match_integer.to_string()));

                        let new_filename = format!("{}-{}", de_suffixed_filename, (match_integer + 1).to_string())
                            .as_str()
                            .parse()
                            .unwrap();

                        return check_filename(new_filename);
                    }
                    Err(_error) => {
                        let new_filename = (&format!("{}-1", filename))
                            .parse()
                            .unwrap();

                        return check_filename(new_filename);
                    }
                }
            }
            None => {
                eprintln!("Something went wrong when getting match");

                "STANDIN_FILENAME".to_string()
            }
        }
    }
}

fn remove_suffix<'a>(string: &'a str, pattern: &str) -> &'a str {
    if string.ends_with(pattern) {
        &string[..string.len() - pattern.len()]
    } else {
        string
    }
}
